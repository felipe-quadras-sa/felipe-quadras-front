import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SessaoInicialComponent } from './sessao-inicial.component';

describe('SessaoInicialComponent', () => {
  let component: SessaoInicialComponent;
  let fixture: ComponentFixture<SessaoInicialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SessaoInicialComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SessaoInicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
